package com.example.bankapp.network;

import com.example.bankapp.accounts.AccountResponse;
import com.example.bankapp.accounts.SaldosResponse;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.moshi.MoshiConverterFactory;
import retrofit2.http.GET;

public class ApiService {

    public static Retrofit instance;
    private static String BASE_URL = "http://bankapp.endcom.mx/api/bankappTest/";
    public static GET_INTERFACE getMethods = getInstance().create(GET_INTERFACE.class);

    public static synchronized Retrofit getInstance(){
        if(instance == null)
            instance = new Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(MoshiConverterFactory.create()).build();

        return instance;
    }

    public interface GET_INTERFACE {

        @GET("cuenta")
        Call<AccountResponse> GetAccount();

        @GET("saldos")
        Call<SaldosResponse> GetSaldos();

        @GET("tarjetas")
        void GetTarjetas();

        @GET("movimientos")
        void GetMovimientos();
    }
}
