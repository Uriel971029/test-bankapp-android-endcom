package com.example.bankapp;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.bankapp.accounts.Account;
import com.example.bankapp.accounts.AccountDetail;
import com.example.bankapp.accounts.AccountResponse;
import com.example.bankapp.accounts.AccountsAdapter;
import com.example.bankapp.accounts.SaldosResponse;
import com.example.bankapp.databinding.FragmentAccountsBinding;
import com.example.bankapp.network.ApiService;
import com.example.bankapp.viewmodels.AccountViewModel;

import java.util.List;

import retrofit2.Retrofit;

public class AccountsFragment extends Fragment {
    FragmentAccountsBinding binding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_accounts, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        AccountViewModel viewModel = new ViewModelProvider(this).get(AccountViewModel.class);
        AccountsAdapter adapter = new AccountsAdapter(new DiffUtil.ItemCallback<AccountDetail>() {
            @Override
            public boolean areItemsTheSame(@NonNull AccountDetail oldItem, @NonNull AccountDetail newItem) {
                return oldItem.id == newItem.id;
            }

            @Override
            public boolean areContentsTheSame(@NonNull AccountDetail oldItem, @NonNull AccountDetail newItem) {
                return oldItem.equals(newItem);
            }
        });

        viewModel.getList().observe(getActivity(), accountDetails ->
        {
            adapter.submitList(accountDetails);
        });
        binding.rvAccounts.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, true));
        binding.rvAccounts.setAdapter(adapter);
        binding.setVm(viewModel);
        binding.setLifecycleOwner(getActivity());
        binding.txtAddCard.setOnClickListener(v ->
        {
                Intent intent = new Intent(getActivity(), AddCardActivity.class);
                startActivity(intent);
        });
    }

}