package com.example.bankapp.accounts;

public class Account {

    public String cuenta;
    public String nombre;
    public String ultimaSesion;
    public int id;

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUltimaSesion() {
        return ultimaSesion;
    }

    public void setUltimaSesion(String ultimaSesion) {
        this.ultimaSesion = ultimaSesion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account(String cuenta, String nombre, String ultimaSesion, int id) {
        this.cuenta = cuenta;
        this.nombre = nombre;
        this.ultimaSesion = ultimaSesion;
        this.id = id;
    }

    public boolean equals(Account account) {
        if(cuenta.equals(account.cuenta))
            return true;
        else
            return false;
    }
}
