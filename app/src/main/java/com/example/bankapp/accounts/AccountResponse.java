package com.example.bankapp.accounts;

import java.util.List;

public class AccountResponse {

    public List<Account> cuenta;

    public AccountResponse(List<Account> cuenta) {
        this.cuenta = cuenta;
    }

    public List<Account> getCuenta() {
        return cuenta;
    }

    public void setCuenta(List<Account> cuenta) {
        this.cuenta = cuenta;
    }
}
