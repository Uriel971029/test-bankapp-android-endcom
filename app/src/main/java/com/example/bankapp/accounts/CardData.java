package com.example.bankapp.accounts;

public class CardData {

    public String number;
    public String account;
    public int cvv;
    public String holder;
    public String brand;
    public String status;
    public float balance;
    public String cardType;

    public CardData(String number, String account, int cvv, String holder, String brand, String status, float balance, String cardType) {
        this.number = number;
        this.account = account;
        this.cvv = cvv;
        this.holder = holder;
        this.brand = brand;
        this.status = status;
        this.balance = balance;
        this.cardType = cardType;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getBalance() {
        return balance;
    }

    public void setBalance(float balance) {
        this.balance = balance;
    }

    public String getCardType() {
        return cardType;
    }

    public void setCardType(String cardType) {
        this.cardType = cardType;
    }
}
