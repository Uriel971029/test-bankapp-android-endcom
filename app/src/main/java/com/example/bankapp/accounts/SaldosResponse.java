package com.example.bankapp.accounts;

import java.util.List;

public class SaldosResponse {

    public List<AccountDetail> saldos;

    public SaldosResponse(List<AccountDetail> saldos) {
        this.saldos = saldos;
    }

    public List<AccountDetail> getSaldos() {
        return saldos;
    }

    public void setSaldos(List<AccountDetail> saldos) {
        this.saldos = saldos;
    }
}
