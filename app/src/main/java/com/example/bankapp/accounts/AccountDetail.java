package com.example.bankapp.accounts;

import androidx.annotation.Nullable;

public class AccountDetail {

    public String cuenta;
    public String saldoGeneral;
    public float ingresos;
    public float gastos;
    public int id;

    public AccountDetail(String cuenta, String saldoGeneral, float ingresos, float gastos, int id) {
        this.cuenta = cuenta;
        this.saldoGeneral = saldoGeneral;
        this.ingresos = ingresos;
        this.gastos = gastos;
        this.id = id;
    }

    public String getCuenta() {
        return cuenta;
    }

    public void setCuenta(String cuenta) {
        this.cuenta = cuenta;
    }

    public String getSaldoGeneral() {
        return saldoGeneral;
    }

    public void setSaldoGeneral(String saldoGeneral) {
        this.saldoGeneral = saldoGeneral;
    }

    public float getIngresos() {
        return ingresos;
    }

    public void setIngresos(float ingresos) {
        this.ingresos = ingresos;
    }

    public float getGastos() {
        return gastos;
    }

    public void setGastos(float gastos) {
        this.gastos = gastos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        return super.equals(obj);
    }
}
