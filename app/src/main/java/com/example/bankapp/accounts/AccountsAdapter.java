package com.example.bankapp.accounts;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.example.bankapp.databinding.AccountAdapterBinding;

public class AccountsAdapter extends ListAdapter<AccountDetail, AccountsAdapter.AccountViewHolder> {

    public AccountsAdapter(@NonNull DiffUtil.ItemCallback<AccountDetail> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public AccountViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        AccountAdapterBinding binding = AccountAdapterBinding.inflate(LayoutInflater.from(parent.getContext()));
        return new AccountViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull AccountViewHolder holder, int position) {
        AccountDetail accountDetail = getItem(position);
        if(position == 0)
            holder.text.setText("Saldo general en cuentas");
        else
            holder.text.setText("Total de ingresos");

        holder.saldo.setText(accountDetail.saldoGeneral);
    }

    public class AccountViewHolder extends RecyclerView.ViewHolder {
        TextView text, saldo;
        public AccountViewHolder(@NonNull AccountAdapterBinding binding) {
            super(binding.getRoot());
            text = binding.txtAccount;
            saldo = binding.txtSaldo;
        }
    }
}
