package com.example.bankapp.viewmodels;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.bankapp.accounts.CardData;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

public class AddCardViewModel extends ViewModel {

    private final Context context;

    public AddCardViewModel(Context context){
        this.context = context;
    }

    public void SenData(CardData cardData){
        Moshi moshi = new Moshi.Builder().build();
        JsonAdapter<CardData> jsonAdapter = moshi.adapter(CardData.class);
        String json = jsonAdapter.toJson(cardData);
        CreateAlert(json);
    }

    private void CreateAlert(String json) {
        Activity activity = (Activity) context;
        new AlertDialog.Builder(activity)
                .setTitle("Información de la tarjeta")
                .setMessage(json)
                .setPositiveButton("Aceptar", (dialog, which) -> {
                   dialog.dismiss();
                }).show();
    }
    
    public static class AddCardViewModelFactory implements ViewModelProvider.Factory {

        private final Context context;

        public AddCardViewModelFactory(Context application) {
            this.context = application;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {

            if(modelClass.isAssignableFrom(AddCardViewModel.class))
                return (T) new AddCardViewModel(context);
            else
                throw new IllegalArgumentException("Cannot create accountViewModel");
        }
    }
    
}
