package com.example.bankapp.viewmodels;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.bankapp.accounts.AccountResponse;
import com.example.bankapp.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainViewModel extends ViewModel {

    public MainViewModel() {
        GetUserData();
    }

    private MutableLiveData<String> name;

    public LiveData<String> getName() {
        if(name == null){
           name = new MutableLiveData<>();
        }
        return name;
    }

    private MutableLiveData<String> lastSession;

    public LiveData<String> getLastSession() {
        if(lastSession == null){
            lastSession = new MutableLiveData<>();
        }
        return lastSession;
    }

    public void GetUserData(){
        Call<AccountResponse> call = ApiService.getMethods.GetAccount();
        call.enqueue(new Callback<AccountResponse>() {
            @Override
            public void onResponse(Call<AccountResponse> call, Response<AccountResponse> response) {
                name.setValue(response.body().cuenta.get(0).getNombre());
                lastSession.setValue(response.body().cuenta.get(0).getUltimaSesion());
            }

            @Override
            public void onFailure(Call<AccountResponse> call, Throwable t) {
                Log.d("error_service", t.getMessage());
            }
        });
    }

}
