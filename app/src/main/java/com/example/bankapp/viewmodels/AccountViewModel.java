package com.example.bankapp.viewmodels;

import android.content.Intent;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.example.bankapp.AddCardActivity;
import com.example.bankapp.accounts.AccountDetail;
import com.example.bankapp.accounts.SaldosResponse;
import com.example.bankapp.network.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AccountViewModel extends ViewModel {

    private MutableLiveData<List<AccountDetail>> _list = new MutableLiveData<>();

    public LiveData<List<AccountDetail>> getList() {
        return _list;
    }

    public AccountViewModel() {
        GetSaldos();
    }

    public void GetSaldos(){

        Call<SaldosResponse> call = ApiService.getMethods.GetSaldos();
        call.enqueue(new Callback<SaldosResponse>() {
            @Override
            public void onResponse(Call<SaldosResponse> call, Response<SaldosResponse> response) {
             _list.setValue(response.body().saldos);
            }

            @Override
            public void onFailure(Call<SaldosResponse> call, Throwable t) {
                Log.d("error_service", t.getMessage());
            }
        });
    }
}
