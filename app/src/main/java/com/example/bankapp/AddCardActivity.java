package com.example.bankapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.bankapp.accounts.CardData;
import com.example.bankapp.databinding.ActivityAddCardBinding;
import com.example.bankapp.viewmodels.AddCardViewModel;

public class AddCardActivity extends AppCompatActivity {
    ActivityAddCardBinding binding;
    AddCardViewModel viewModel;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_card);
        viewModel = new ViewModelProvider(this, new AddCardViewModel.AddCardViewModelFactory(AddCardActivity.this)).get(AddCardViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setVm(viewModel);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.btnAdd.setOnClickListener(v -> {
            try {
                viewModel.SenData(
                        new CardData(
                                binding.txtNumber.getText().toString(),
                                binding.txtAccount.getText().toString(),
                                Integer.parseInt(binding.txtCvv.getText().toString()),
                                binding.txtHolder.getText().toString(),
                                binding.txtBrand.getText().toString(),
                                binding.txtStatus.getText().toString(),
                                Float.parseFloat(binding.txtBalance.getText().toString()),
                                binding.txtCardType.getText().toString())
                );
            }catch (Exception ex){
                Toast.makeText(AddCardActivity.this, "Complete los campos", Toast.LENGTH_SHORT).show();
            }
        });
    }
}